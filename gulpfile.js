let gulp = require('gulp');
let sourcemaps = require('gulp-sourcemaps');
let sass = require('gulp-sass');
let cssClassPrefix = require('gulp-css-class-prefix');
let browserSync = require('browser-sync').create();


let scss_config = {
    src: './scss/**/*.scss',
    dest: './css'
};

gulp.task('scss', function(){
    return gulp.src(scss_config.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(scss_config.dest))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('prefixCss', function() {
    return true;
    return gulp.src('./css/*.css')
    //return gulp.src(`${scss_config.dest}/*.css`)
        .pipe(cssClassPrefix('my-class-prefix-'))
        .pipe(gulp.dest(scss_config.dest))
});

gulp.task('watch', ['browserSync', 'scss', 'prefixCss'], function() {
    gulp.watch(scss_config.src, ['scss', 'prefixCss']);
    gulp.watch('./*.html', browserSync.reload);
    gulp.watch('./css/**/*.css', browserSync.reload);
    gulp.watch('./js/**/*.js', browserSync.reload);
});

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: './'
        },
    })
});